import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';

const StripeCheckoutButton = ({ price }) => {
    const priceForStripe = price * 100;
    const publishableKey = 'pk_test_51H0CHjGcSvXs4ZF2vSsNkpeOmBWHRaPmdDLpTvlNvDsZxoVOFXUwQOwF2wE8iV7U54z8VwM8MH4w4nnj495e29fj00SJswpple';

    const onToken = token => {
        axios({
            url: 'payment',
            method: 'post',
            data: {
                amount: priceForStripe,
                token
            }
        }).then(response => {
            alert('Payment Successful')
        }).catch(error => {
            console.log('Payment error: ', JSON.parse(error));
            alert('Payment Error');
        })
    }

    return (
        <StripeCheckout
            label='Pay Now'
            name='CRWN CLOTHING Ltd.'
            billingAddress
            shippingAddress
            image='https://svgshare.com/i/CUz.svg'
            description={`Your total is $${price}`}
            amount={priceForStripe}
            panelLabel='Pay Now'
            token={onToken}
            stripeKey={publishableKey}
        />
    )
};

export default StripeCheckoutButton;