import React from 'react';

import { CheckoutItemContainer, ImageContainer, NameQtyPriceContainer, ArrowContainer, ValueContainer, RemoveButtonContainer } from './checkout-item.styles';
import { connect } from 'react-redux';
import { clearItemFromCart, removeItem, addItem } from '../../redux/cart/cart.actions';

const CheckoutItem = ({ cartItem, addItem, removeItem, clearItem }) => {
    const { name, imageUrl, price, quantity } = cartItem;
    return (<CheckoutItemContainer>
        <ImageContainer>
            <img src={imageUrl} alt='item' />
        </ImageContainer>
        <NameQtyPriceContainer>{name}</NameQtyPriceContainer>
        <NameQtyPriceContainer>
            <ArrowContainer onClick={() => removeItem(cartItem)}>&#10094;</ArrowContainer>
            <ValueContainer>{quantity}</ValueContainer>
            <ArrowContainer onClick={() => addItem(cartItem)}>&#10095;</ArrowContainer>
        </NameQtyPriceContainer>
        <NameQtyPriceContainer>${price}</NameQtyPriceContainer>
        <RemoveButtonContainer onClick={() => clearItem(cartItem)}>
            &#10005;
        </RemoveButtonContainer>
    </CheckoutItemContainer>)
};

const mapDispacthToProps = dispatch => ({
    clearItem: item => dispatch(clearItemFromCart(item)),
    addItem: item => dispatch(addItem(item)),
    removeItem: item => dispatch(removeItem(item))
});

export default connect(null, mapDispacthToProps)(CheckoutItem);